#!/usr/bin/python3
import subprocess

import gitlab

glab = gitlab.Gitlab(f'https://git.nonprod.williamhill.plc', f'MeCdzyafzLSF-D3xxR8s', None, None, False, None, None, None, "4", None, None)
groups = glab.groups.list(all=True)
groupname = 'trading_development'
projects = []
for group in groups:
    if groupname in group.name:
        projects = projects + group.projects.list(all=True)
        subgroups1 = group.subgroups.list(all=True)
        for subgroup1 in subgroups1:
            subgroup1_object = glab.groups.get(subgroup1.id, lazy=True)
            projects = projects + subgroup1_object.projects.list(all=True)
            subgroups2 = subgroup1_object.subgroups.list(all=True)
            for subgroup2 in subgroups2:
                subgroup2_object = glab.groups.get(subgroup2.id, lazy=True)
                projects = projects + subgroup2_object.projects.list(all=True)
                subgroups3 = subgroup2_object.subgroups.list(all=True)
                for subgroup3 in subgroups3:
                    subgroup3_object = glab.groups.get(subgroup3.id, lazy=True)
                    projects = projects + subgroup3_object.projects.list(all=True)
                    subgroups4 = subgroup3_object.subgroups.list(all=True)
                    for subgroup4 in subgroups4:
                        subgroup4_object = glab.groups.get(subgroup4.id, lazy=True)
                        projects = projects + subgroup4_object.projects.list(all=True)
                        subgroups5 = subgroup4_object.subgroups.list(all=True)
                        for subgroup5 in subgroups5:
                            subgroup5_object = glab.groups.get(subgroup5.id, lazy=True)
                            projects = projects + subgroup4_object.projects.list(all=True)

counter = 0
for repo in projects:
    if not repo.archived:
        counter += 1
        folder = repo.path_with_namespace.replace('/'+repo.name, '')
        command = f"cd ~/dev/repos && mkdir -p {folder} && cd {folder} && git clone {repo.ssh_url_to_repo} "
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        output, _ = process.communicate()
        process.wait()
print('We clone %d projects'%counter)
